package libasoles.processing.board.projects.DemoProject;

import libasoles.processing.board.framework.Main;
import libasoles.processing.board.projects.DemoProject.sketches.eye.Eye;
import libasoles.processing.board.projects.DemoProject.sketches.knobs.Knobs;
import processing.core.PApplet;

public class DemoProject extends Main {

	public static void main(String args[]) {
		PApplet.main(new String[] { "libasoles.processing.board.projects.DemoProject.DemoProject" }); // self
	}		
	
	@Override
	public void loadSketches() {

		// add sketches
		sm.add( new Eye(this) );	
		sm.add( new Knobs(this) );	
	}
}
