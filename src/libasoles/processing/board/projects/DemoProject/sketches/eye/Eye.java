package libasoles.processing.board.projects.DemoProject.sketches.eye;

import libasoles.processing.board.framework.sketches.Sketch;
import libasoles.processing.board.framework.utils.windows.SketchWindow;
import processing.core.PApplet;
import processing.core.PConstants;
import processing.event.MouseEvent;

public class Eye extends Sketch{
	float centerX;
    float centerY;
    float outlineDiameter = 550f;
    float outlineRadious = outlineDiameter / 2.0f;
    float pupilDiameter = 280f;
    float pupilRadious = pupilDiameter / 2.0f;

    float mouseX, mouseY;
    
    public Eye(SketchWindow applet) {
        super(applet);
        name = "Eye";
    }
    
    @Override
    public void setup() {
    	super.setup();
        centerX = applet.width / 2.0f;
        centerY = applet.height / 2.0f;
    }

    @Override
    public void draw() {
        applet.background(0);
        drawOutline();
        drawPupil();
    }

    public void drawOutline() {
        applet.stroke(0);
        applet.strokeWeight(2);
        applet.fill(255);
        applet.arc(centerX, centerY, outlineDiameter, outlineDiameter, 0, 2.0f * PConstants.PI);
    }

    public void drawPupil() {
        applet.noStroke();
        applet.fill(150);
        float pupilX, pupilY;
        
        float distance = calculateDistance(centerX, centerY, mouseX, mouseY);
        if (distance <= outlineRadious - pupilRadious) {
        	pupilX = mouseX;
            pupilY = mouseY;     
            applet.arc(mouseX, mouseY, pupilDiameter, pupilDiameter, 0, 2.0f * PConstants.PI);
        } else {
            float cos = (mouseX - centerX) / distance;
            float sin = (mouseY - centerY) / distance;
            pupilX = centerX + (outlineRadious - pupilRadious) * cos;
            pupilY = centerY + (outlineRadious - pupilRadious) * sin;            
            applet.arc(pupilX, pupilY, pupilDiameter, pupilDiameter, 0, 2.0f * PConstants.PI);            
        }
        
        applet.fill(30);
        applet.ellipse(pupilX, pupilY, pupilDiameter/2, pupilDiameter/2);
        
        applet.fill(255);
        applet.ellipse(pupilX+pupilDiameter/4+4, pupilY-pupilDiameter/4-4, pupilDiameter/4-2, pupilDiameter/4-2);
    }

    public float calculateDistance(float x1, float y1, float x2, float y2) {
        return PApplet.sqrt(PApplet.sq(x1 - x2) + PApplet.sq(y1 - y2));
    }
    
    public void mouseMoved(MouseEvent event) {
    	mouseX = event.getX();
    	mouseY = event.getY();
	}
}
