package libasoles.processing.board.projects.DemoProject.sketches.knobs;

import libasoles.processing.board.framework.sketches.Sketch;
import libasoles.processing.board.framework.utils.windows.SketchWindow;
import processing.core.PApplet;

public class Knobs extends Sketch {

	int bg = 0;
	
	public Knobs(SketchWindow applet) {
		super(applet);	
	}

	@Override
	public void setup() {
		
		description+= "Mover las perillas para controlar las variables del sketch";
						
		controlWindow.addControl("knob", "bgColor", 255, 0);
		controlWindow.addControl("knob", "circlePositionX", applet.width, applet.width/2);
		controlWindow.addControl("knob", "circlePositionY", applet.height, applet.height/2);
		controlWindow.addControl("knob", "circleRadius", 400, 200);
		controlWindow.addControl("knob", "circleColor", 255, 255);
		controlWindow.addControl("knob", "num", 100, 10);
		controlWindow.addControl("knob", "numColor", 255, 10);
		controlWindow.addControl("knob", "numSize", 100, 30);
			
		controlWindow.addControl("knob", "on-off", 1, 1);
		controlWindow.addControl("knob", "left-middle-right", 2, 1);
			
		applet.ellipseMode(PApplet.CENTER);
		applet.textAlign(PApplet.CENTER, PApplet.CENTER);
	}

	public void draw() {
	
		applet.background( getKnobValue("bgColor") );
				
		applet.noStroke();
		applet.fill(getKnobValue("circleColor"));
		applet.ellipse( getKnobValue("circlePositionX"), getKnobValue("circlePositionY"), getKnobValue("circleRadius"), getKnobValue("circleRadius") );
		
		applet.fill(getKnobValue("numColor"));
		applet.stroke(getKnobValue("numColor"));		
		applet.textSize(getKnobValue("numSize"));
		applet.strokeWeight(4);
		
		applet.text((int)getKnobValue("num"), getKnobValue("circlePositionX"), getKnobValue("circlePositionY"));
		
		int on = (int) getKnobValue("on-off");
		if(on == 1){
			
			int color = (int) getKnobValue("left-middle-right");
			switch (color) {
			case 0:
				color = applet.color(255, 0, 0);
				break;
			case 1:
				color = applet.color(0, 255, 0);
				break;
			default:
				color = applet.color(0, 0, 255);
				break;
			}
			applet.fill(color);
			
			float f = PApplet.radians(applet.frameCount%360);
			applet.ellipse( getKnobValue("circlePositionX") + PApplet.cos(f)*getKnobValue("circleRadius"),
					getKnobValue("circlePositionY") + PApplet.sin(f)*getKnobValue("circleRadius"), 40, 40 );
		}
	}
}
