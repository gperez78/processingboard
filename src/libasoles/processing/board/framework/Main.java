package libasoles.processing.board.framework;

import libasoles.processing.board.framework.utils.windows.ControlWindow;
import libasoles.processing.board.framework.utils.windows.SketchWindow;
import libasoles.processing.board.framework.utils.windows.Window;
import processing.core.PApplet;
import processing.core.PImage;

public class Main extends SketchWindow {

	public ControlWindow controlWindow; 

	/*public static void main(String args[]) {
		PApplet.main(new String[] { "libasoles.processing.framework.Main" });
	}*/

	@Override
	public void settings() {

		size(800, 600, P2D);
		// fullScreen(P2D, 1); // projector must be 800x600

		smooth(4);
	}

	@Override
	public void setup() {
		
		
		Window.mirror = new PImage(800, 600); // main window live sync image

		// control window
		String[] args = { "--display=1", "Control Box Window" };
		controlWindow = new ControlWindow(this);
		PApplet.runSketch(args, controlWindow);
		
		loadSketches();
		sm.setCurrentSketch(0); // will autoplay current sketch		
		
		controlWindow.listSketches();
		controlWindow.init();		
	}

	@Override
	public void draw() {
		sm.draw();
	}
	
	protected void loadSketches() {
		System.out.println("No sketches loaded. Please add some.");
	}
}
