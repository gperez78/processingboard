package libasoles.processing.board.framework.utils;

import java.lang.reflect.InvocationTargetException;

import libasoles.processing.board.framework.utils.windows.Window;
import processing.core.PApplet;
import processing.core.PFont;

/* Derivated work from: http://www.openprocessing.org/sketch/58978 */
public class Radio {

  java.lang.reflect.Method method;	
  String callback;	
  
  int externalCircleColor=0;
  int externalFillCircleColor=255;
  int internalCircleColor=0;
  int internalFillCircleColor=0;
  
  boolean fillExternalCircle=false;
  
  PFont rText;
  int textColor=0;
  int textShadowColor=100;
  boolean textShadow=false;
  int textPoints=14;
  
  int xTextOffset=20;
  int yTextSpacing=14;
  
  int circleRadius=16;
  float circleLineWidth=0.5f;
 
  float boxLineWidth=0.2f;
  boolean boxFilled=false;
  int boxLineColor=0;
  int boxFillColor=150;
  boolean boxVisible=true;
  
  String[] radioText;
  boolean[] radioChoose; 
  
  int over=0;
  int nC;
  
  int rX, rY;
  
  float maxTextWidth=0;
  
  String radioLabel;
  
  boolean debug=false;
  
  int boxXMargin=5;
  int boxYMargin=5;
  
  int bX, bY, bW, bH;
  boolean pressOnlyOnce=true;
  int deb=0;    
  
  Window applet;
      
  ///////////////////////////////////////////////////////  
  public Radio(Window applet, int x, int y, String[] op, String id)
  {
    this.applet = applet;
    
    rX=x;
    rY=y;
    radioText=op;
    
    nC=op.length;
    radioChoose = new boolean[nC];
        
    rText = applet.createFont("Arial",16,true);      
    applet.textFont(rText,textPoints);   
    applet.textAlign(PApplet.LEFT);
    
    for (int i=0; i<nC; i++) 
    {
      if (applet.textWidth(radioText[i]) > maxTextWidth) maxTextWidth=applet.textWidth(radioText[i]);
      radioChoose[i]=false;
    }
    
    if(radioChoose.length > 0)
    	radioChoose[over]=true;
    
    radioLabel=id;
    
    calculateBox();
    
  }
  
  public void setCallback(String callback){
	  this.callback = callback;
  }
  
///////////////////////////////////////////////////////  
  public void calculateBox()
  {
    bX=rX-circleRadius/2-boxXMargin;
    bY=rY-circleRadius/2-boxYMargin;
    bW=circleRadius*2+xTextOffset+(int )maxTextWidth;
    bH=radioText.length*circleRadius + (radioText.length-1)*yTextSpacing + boxYMargin*2;
  }  
///////////////////////////////////////////////////////  
  public void setValue(int n)
  {
    if (n<0) n=0;
    if (n>(nC-1)) n=nC-1;
    
   for (int i=0; i<nC; i++) 
	   radioChoose[i]=false;
   
   if(radioChoose.length > 0)
	   radioChoose[n]=true;  
   over=n; 
  }
///////////////////////////////////////////////////////  
  public void deBounce(int n)
  {
    if (pressOnlyOnce) 
      return;
    else
      
    if (deb++ > n) 
    {
      deb=0;
      pressOnlyOnce=true;
    }
    
  }  ///////////////////////////////////////////////////////  
  public boolean mouseOver()
  {
    boolean result=false; 
    
    if (debug)
      if ((applet.mouseX>=bX) && (applet.mouseX<=bX+bW) && (applet.mouseY>=bY) && (applet.mouseY<=bY+bH))
      {
        if (applet.mousePressed && applet.mouseButton==PApplet.LEFT && applet.keyPressed)
        {
          if (applet.keyCode==PApplet.CONTROL)
          {
            rX=rX+(int )((float )(applet.mouseX-applet.mouseX)*1);
            rY=rY+(int )((float )(applet.mouseY-applet.mouseY)*1);
            calculateBox();
          }
          if (applet.keyCode==PApplet.SHIFT && pressOnlyOnce) 
          {
            printGeometry();
            pressOnlyOnce=false;
          }
          deBounce(5);
          
        }
      }
      
    for (int i=0; i<nC; i++)
    {
      if ((applet.mouseX>=(rX-circleRadius)) && (applet.mouseX<=(rX+circleRadius)) && (applet.mouseY>=(rY+(i*(yTextSpacing+circleRadius))-circleRadius)) && (applet.mouseY<=(rY+(i*(yTextSpacing+circleRadius))+circleRadius)))
      {
        result=true;
        
        if (applet.mousePressed && applet.mouseButton==PApplet.LEFT && pressOnlyOnce)
        {
        	
          over=i;	
        	
          if(callback != null){
        	  
        	try {
				method = applet.getClass().getMethod(callback, int.class);
			} catch (NoSuchMethodException e) {
				e.printStackTrace();
			} catch (SecurityException e) {
				e.printStackTrace();
			}
        	
    	    try {
    	      Object callBackResult = (boolean) method.invoke(applet, i);
    		        
    	      if((boolean) callBackResult == true)
    	    	  setValue(over);
    		  
    		} catch (IllegalArgumentException e) {
    		} catch (IllegalAccessException e) {
    		} catch (InvocationTargetException e) {        			
    		}
        	  
          }        	  

          pressOnlyOnce=false;
        }
        deBounce(5);
        i=nC;
      }
      else
      {
        result=false;
      }
    } 
    return result;
  }
///////////////////////////////////////////////////////  
  public void drawBox()
  {
    if (!boxVisible) return;
    if (boxFilled)
      applet.fill(boxFillColor);
    else
    	applet.noFill();
    applet.strokeWeight(boxLineWidth);
    applet.stroke(boxLineColor);

    applet.rect(bX, bY, bW, bH);

  }  
///////////////////////////////////////////////////////  
  public void drawCircles()
  {
    applet.strokeWeight(circleLineWidth);
    for (int i=0; i<nC; i++)
    {
      if (!fillExternalCircle) 
    	  applet.noFill();
      else
        applet.fill(externalFillCircleColor);  
      applet.stroke(externalCircleColor);  
      applet.ellipse(rX, rY+(i*(yTextSpacing+circleRadius)), circleRadius, circleRadius);

      applet.fill(internalFillCircleColor);
      applet.stroke(internalCircleColor);  

      if (radioChoose[i])
    	  applet.ellipse(rX, rY+(i*(yTextSpacing+circleRadius)), circleRadius-8, circleRadius-8);
    }
    mouseOver();
   
  }
///////////////////////////////////////////////////////  
  public void drawText()
  {
    float yOffset=rY+textPoints/3+1;
    applet.stroke(textColor);
    applet.textFont(rText,textPoints);   
    applet.textAlign(PApplet.LEFT);

    for (int i=0; i<nC; i++)
    {
      if (textShadow)
      {
        applet.stroke(textShadowColor);
        applet.text(radioText[i], rX+xTextOffset+1, yOffset+(i*(yTextSpacing+circleRadius))+1);
        applet.stroke(textColor);
      }
      applet.text(radioText[i], rX+xTextOffset, yOffset+(i*(yTextSpacing+circleRadius)));
    }
    
  }  
  
///////////////////////////////////////////////////////  
  public int update()
  {
    drawBox();
    drawCircles();
    drawText();
    
    return over;
  }

///////////////////////////////////////////////////////  
  public int getValue()
  {
    return over;
  }
 
///////////////////////////////////////////////////////  
  public void setDebugOn()
  {
    debug=true;
  }
///////////////////////////////////////////////////////  
  public void setDebugOff()
  {
    debug=false;
  }
///////////////////////////////////////////////////////  
  public void printGeometry()
  {
    PApplet.println("radio = new radio("+rX+", "+rY+", arrayOfOptions"+", \""+radioLabel+"\");");
  }
///////////////////////////////////////////////////////  
  public void setExternalCircleColor(int c)
  {
    externalCircleColor=c;
  }
///////////////////////////////////////////////////////  
  public void setExternalFillCircleColor(int c)
  {
    externalFillCircleColor=c;
  }
///////////////////////////////////////////////////////  
  public void setInternalCircleColorr(int c)
  {
    externalFillCircleColor=c;
  }
///////////////////////////////////////////////////////  
  public void setInternalFillCircleColor(int c)
  {
    externalFillCircleColor=c;
  }
///////////////////////////////////////////////////////  
  public void setTextColor(int c)
  {
    textColor=c;
  }
///////////////////////////////////////////////////////  
  public void setTextShadowColor(int c)
  {
    textShadowColor=c;
  }
///////////////////////////////////////////////////////  
  public void setShadowOn()
  {
    textShadow=true;
  }
///////////////////////////////////////////////////////  
  public void setShadowOff()
  {
    textShadow=false;
  }
///////////////////////////////////////////////////////  
  public void setTextSize(int s)
  {
    textPoints=s;
  }
///////////////////////////////////////////////////////  
  public void setXTextOffset(int s)
  {
    xTextOffset=s;
  }
///////////////////////////////////////////////////////  
  public void setyTextSpacing(int s)
  {
    yTextSpacing=s;
  }
///////////////////////////////////////////////////////  
  public void setCircleRadius(int s)
  {
    circleRadius=s;
  }
///////////////////////////////////////////////////////  
  public void setBoxLineWidth(int s)
  {
    boxLineWidth=s;
  }
///////////////////////////////////////////////////////  
  public void setBoxLineColor(int c)
  {
    boxLineColor=c;
  }
///////////////////////////////////////////////////////  
  public void setBoxFillColor(int c)
  {
    boxFillColor=c;
    setBoxFilledOn();
  }
///////////////////////////////////////////////////////  
  public void setBoxFilledOn()
  {
    boxFilled=true;
  }
///////////////////////////////////////////////////////  
  public void setBoxFilledOff()
  {
    boxFilled=false;
  }
///////////////////////////////////////////////////////  
  public void setBoxVisibleOn()
  {
    boxVisible=true;
  }
///////////////////////////////////////////////////////  
  public void setBoxVisibleOff()
  {
    boxVisible=false;
  }
///////////////////////////////////////////////////////  
  public void setLabel(String l)
  {
    radioLabel=l;
  }

}