package libasoles.processing.board.framework.utils;

import libasoles.processing.board.framework.utils.windows.SketchWindow;

public class Drawable{
    
    public SketchWindow applet;
    
    public Drawable(SketchWindow applet) {
        this.applet = applet;
    }
}