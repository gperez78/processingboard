package libasoles.processing.board.framework.utils;

import libasoles.processing.board.framework.utils.windows.SketchWindow;
import processing.core.PVector;

class Widget extends Drawable {

    PVector pos;
    int c;
    boolean hover;

    Widget(SketchWindow project) {
        super(project);
        pos = new PVector();
    }

    Widget(SketchWindow project, float x, float y) {
        this(project);
        setPosition(x, y);
    }

    Widget(SketchWindow project, PVector p) {
        super(project);
        pos = p;
    }

    void setup() {
    }

    void update() {
    }

    void draw() {
    }

    PVector getPosition() {
        return pos;
    }

    void setPosition(float x, float y) {
        pos.x = x;
        pos.y = y;
    }

    void setPosition(PVector p) {
        setPosition(p.x, p.y);
    }

    void setColor(int c) {
        this.c = c;
    }

    int getColor() {
        return c;
    }

    void mouseDragged() {

    }

    void mouseReleased() {

    }
}
