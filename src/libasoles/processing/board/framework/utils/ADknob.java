package libasoles.processing.board.framework.utils;

import libasoles.processing.board.framework.utils.windows.Window;
import processing.core.PApplet;

/*
 * ----------------------------------
 *  Knob Class for Processing 2.0
 * ----------------------------------
 *
 * this is a simple knob class. The following shows 
 * you how to use it in a minimalistic way.
 *
 * DEPENDENCIES:
 *   N/A
 *
 * Created:  March, 19 2012
 * Author:   Alejandro Dirgan
 * Version:  0.35
 *
 * License:  GPLv3
 *   (http://www.fsf.org/licensing/)
 *
 * Follow Us
 *    adirgan.blogspot.com
 *    twitter: @ydirgan
 *    https://www.facebook.com/groups/mmiiccrrooss/
 *    https://plus.google.com/b/111940495387297822358/
 *
 * DISCLAIMER **
 * THIS SOFTWARE IS PROVIDED TO YOU "AS IS," AND WE MAKE NO EXPRESS OR IMPLIED WARRANTIES WHATSOEVER 
 * WITH RESPECT TO ITS FUNCTIONALITY, OPERABILITY, OR USE, INCLUDING, WITHOUT LIMITATION, ANY IMPLIED 
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, OR INFRINGEMENT. WE EXPRESSLY 
 * DISCLAIM ANY LIABILITY WHATSOEVER FOR ANY DIRECT, INDIRECT, CONSEQUENTIAL, INCIDENTAL OR SPECIAL 
 * DAMAGES, INCLUDING, WITHOUT LIMITATION, LOST REVENUES, LOST PROFITS, LOSSES RESULTING FROM BUSINESS 
 * INTERRPApplet.UPTION OR LOSS OF DATA, REGARDLESS OF THE FORM OF ACTION OR LEGAL THEORY UNDER WHICH THE LIABILITY 
 * MAY BE ASSERTED, EVEN IF ADVISED OF THE POSSIBILITY OR LIKELIHOOD OF SUCH DAMAGES.
 
 
EXAMPLE: Using knobs to change the color of the background manipulating R,G,B

ADknob rcolor, gcolor, bcolor;
int r,g,b;

void activateMouseWheel()
{
 addMouseWheelListener(new java.awt.event.MouseWheelListener() 
 { 
   public void mouseWheelMoved(java.awt.event.MouseWheelEvent evt) 
   { 
     mouseWheel(evt.getWheelRotation());
   }
 });

}

void setup()
{
   size(500,200);
   smooth();
   rcolor = new ADknob("R",100,100,45,255,0.0,1.0,4.0,20.0,34.0,19.0,34.0,15,7,53.0,20,19,10,-6,14,5,9,14,7,23,false,11,0);
   rcolor.setKnobPosition(234);
   rcolor.setColors(#D30606,0,-1,0,0,0,0,-11574371,-327681);
   gcolor = new ADknob("G",250,100,45,255,0.0,1.0,4.0,20.0,34.0,19.0,34.0,15,7,53.0,20,19,10,-6,14,5,9,14,7,23,false,11,0);
   gcolor.setKnobPosition(202);
   gcolor.setColors(#0FA705,0,-1,0,0,0,0,-11574371,-327681);
   bcolor = new ADknob("B",400,100,45,255,0.0,1.0,4.0,20.0,34.0,19.0,34.0,15,7,53.0,20,19,10,-6,14,5,9,14,7,23,false,11,0);
   bcolor.setKnobPosition(111);
   bcolor.setColors(#050EA7,0,-1,0,0,0,0,-11574371,-327681);
   
   activateMouseWheel();
}

void draw()
{
  background(color(r,g,b));
  r=(int )rcolor.update();
  g=(int )gcolor.update();
  b=(int )bcolor.update();
}

void mouseWheel(int delta)
{  
  rcolor.changeKnobPositionWithWheel(delta);
  gcolor.changeKnobPositionWithWheel(delta);
  bcolor.changeKnobPositionWithWheel(delta);
}
 
*/
public class ADknob extends ADcontrol
{
  int knobRadius=100;
  int knobX,knobY;
  float knobValue=0;
  int prevKnobPosition=0;
  
  int knobIncrement=1;
  int internalColor;
  int internalColorLight=0xFF0DC6D8+0xFF230F0F;
  float strokeExternalCircle=8;
  
  int strokeOutterIndicator=14;
  int strokeInnerIndicator=6;

  int strokeOutterMarks=3;
  int strokeOutterInitEndMarks=5;
  int knobSteps=6;
    
  int colorExternalCircle=0;
  int colorOutterIndicator=0;
  int colorInnerIndicator=0xFFFFFFFF;
  int internalColorMarks=0;
  int externalColorMarks=0;
  int colorMarksLight=0xFFFF4400;
  int knobPlusStroke=5;
  
  int colorText=0;
  
  float angle=0;
  
  float outterOffsetFromCenter=10;
  float knobOutterLength=20;
  
  float innerOffsetFromCenter=10;
  float knobInnerLength=20;
  
  boolean knobmouseOver=false;

  float knobRightAngle=30; 
  int knobLengthMarkOuter=15;
  int knobLengthMarkInner=5;
  
  float knobMaxIniAngle;
  float knobMaxEndAngle;
  float knobTotalAngle;
  
  float[] knobStops;  
  int knobPosition;
  
  float knobStartValue;
  float knobEndValue;
  float knobSum;
  
  boolean knobSnap=true;
  
  float knobActualAngle;
  
  boolean knobKeyPressed=false;
  
  int knobValueTextSize=10;
  int knobColorValueText=0xFFFF4400;
  int knobLabelTextSize=16;
  int knobColorLabelText=0xFF4F639D;
  
  String knobLabelText;
  int knobLabelOffset=15;
  int knobValueOffset=30;
  boolean knobLabelTextVisible=true;
  
  float knobIndicatorOffset=0;
  
  int knobSkips=50;
  int knobNumberOfMarks=10;
  
  int knobOffsetMarks=10;
  
  int konbInnerIndicatorHigh=0xFFFFE200;
  
  int knobValuelabelSize=11;
  int knobColorValueLabel=0;
  
  int knobValueLabelOffset=5;
  boolean showKnobLabelValues=false;
  
  int valueLabelOffset=0;
  
  boolean pressOnlyOnce=true;
  boolean debug=false;
  int deb=0;
  
  boolean knobMoving=false;

  public String group;
  boolean visible = true;
  
Window applet;
  
  public ADknob(Window applet, String knobName, int x, int y, int r, int stops, float vi, float step)
  {
	this.applet = applet;
	
	internalColor=applet.color(13,198,216);
	
    knobX=x;
    knobY=y;
    knobRadius=r;
    knobMaxIniAngle=(180-knobRightAngle)*PApplet.PI/180;
    knobMaxEndAngle=knobRightAngle*PApplet.PI/180+PApplet.TWO_PI;
    knobTotalAngle=2*knobRightAngle*PApplet.PI/180+PApplet.PI;
    knobSteps=stops;
    
    knobStops =  new float[knobSteps+1];

    knobStartValue=vi;
    knobValue=knobStartValue;
    knobSum=step;
    calculateStops();
    
    //Initial Position
    knobPosition=prevKnobPosition=0;
    angle=knobActualAngle=knobStops[knobPosition];
    
    knobLabelText=knobName;
    
    if (stops>10) knobSkips=stops/knobNumberOfMarks;

  }
  
  public ADknob(Window applet, String knobName, int x, int y, int r, int stops, float vi, float step, float eec,  float oofc, float ol, float iofc, float il, int lmo, int lmi, float a, int ls, int lo, int vs, int vo, int om, int som, int soiem, int soi, int sii, int nom, boolean vvl, int vls, int vlo)
  {
	    //int knobRadius, int strokeExternalCircle,float outterOffsetFromCenter, float knobOutterLength
	    // float innerOffsetFromCenter, float knobInnerLength, int knobLengthMarkOuter, int knobLengthMarkInner, float knobRightAngle
	    // in labelSize, labelOffset, valueSize, valueOffset

		this.applet = applet;

		internalColor = applet.color(13, 198, 216);

		knobX = x;
		knobY = y;
		knobRadius = r;
		knobSteps = stops;

		knobStops = new float[knobSteps + 1];

		knobStartValue = vi;
		knobValue = knobStartValue;
		knobSum = step;

		// Initial Position
		knobPosition = prevKnobPosition = 0;
		angle = knobActualAngle = knobStops[knobPosition];

		knobLabelText = knobName;
		strokeExternalCircle = eec;
		outterOffsetFromCenter = oofc;
		knobOutterLength = ol;
		innerOffsetFromCenter = iofc;
		knobInnerLength = il;
		knobLengthMarkOuter = lmo;
		knobLengthMarkInner = lmi;
		knobRightAngle = a;

		knobLabelTextSize = ls;
		knobLabelOffset = lo;
		knobValueTextSize = vs;
		knobValueOffset = vo;

		knobMaxIniAngle = (180 - knobRightAngle) * PApplet.PI / 180;
		knobMaxEndAngle = knobRightAngle * PApplet.PI / 180 + PApplet.TWO_PI;
		knobTotalAngle = 2 * knobRightAngle * PApplet.PI / 180 + PApplet.PI;
		calculateStops();
		setKnobPosition(knobPosition);

		knobNumberOfMarks = nom;

		if (stops > 10)
			knobSkips = stops / knobNumberOfMarks;

		knobOffsetMarks = om;

		strokeOutterMarks = som;

		strokeOutterIndicator = soi;
		strokeInnerIndicator = sii;
		strokeOutterInitEndMarks = soiem;

		showKnobLabelValues = vvl;

		knobValuelabelSize = vls;

		valueLabelOffset=vlo;

  }

	public String getGroup() {
		return group;
	}

	public void setGroup(String group) {
		this.group = group;
	}
  
	public void show() {
		this.visible = true;
	}
	
	public void hide() {
		this.visible = false;
	}
	
	public boolean isVisible(){
		return visible;
	}
	
  ////////////////////////////////////////////////////////////////////////
  float round2nDecimals(float number, float decimal) {
      return (float)(PApplet.round((number*PApplet.pow(10, decimal))))/PApplet.pow(10, decimal);
  }   
////////////////////////////////////////////////////////////////////////
  void calculateStops()
  {
    float steps = knobTotalAngle/knobSteps;
    float rangle=knobMaxIniAngle;

    for (int i = 0; i <= knobSteps; i++)
    {
      knobStops[i]=rangle;
      rangle+=steps;
    }
    knobEndValue=knobStartValue+knobSteps*knobSum;
    
  }
////////////////////////////////////////////////////////////////////////
  void drawLabels()
  {
    if (!knobLabelTextVisible) return;
    applet.textSize(knobLabelTextSize);
    applet.fill(knobColorLabelText);
    applet.pushMatrix();
    applet.translate(knobX,knobY);
    applet.text(knobLabelText,0,knobRadius+knobLabelOffset);
    applet.popMatrix();
    
  }
////////////////////////////////////////////////////////////////////////
  void drawValues()
  {
    changeKnobValue();
    
    applet.textSize(knobValueTextSize);
    applet.textAlign(PApplet.CENTER);
    applet.fill(knobColorValueText);
    applet.pushMatrix();
    applet.translate(knobX,knobY);
    //applet.text("nnn",-knobRadius-10,knobRadius+10);
    applet.text(PApplet.str(round2nDecimals(knobValue,1)),0,knobRadius+knobValueOffset);
  
    applet.popMatrix();

  }
////////////////////////////////////////////////////////////////////////
  void showLabelValue(int value, int offset)
  {
    if (!showKnobLabelValues) return;
    
    applet.pushMatrix();
    applet.textSize(knobValuelabelSize);
    applet.textAlign(PApplet.CENTER);
    applet.fill(knobColorValueLabel);
    applet.translate(0,0);
    applet.rotate(PApplet.PI/2);
    applet.translate(0,-offset);
    applet.text(PApplet.str(value),0,0-valueLabelOffset);
    applet.popMatrix();
  }
////////////////////////////////////////////////////////////////////////
  void changeKnobValue()
  {
    knobValue=knobStartValue+knobPosition*knobSum;
  }
////////////////////////////////////////////////////////////////////////
  void drawExternalMarks()
  {
   
    int knobMarks=9;
    float ksv=knobStartValue;
    
    applet.pushMatrix();
    applet.translate(knobX,knobY);
       
    for (int i = 0; i < knobSteps+1; i++)
    {

      applet.pushMatrix();
      applet.rotate(knobIndicatorOffset+knobStops[i]);

      if ((i==0) || (i==knobSteps)) 
      {
        applet.stroke(externalColorMarks);
        if (knobPosition==i)
          applet.stroke(colorMarksLight);
          
        applet.strokeWeight(strokeOutterInitEndMarks); 
        applet.line(knobRadius+knobOffsetMarks, 0, knobRadius+knobOffsetMarks+knobLengthMarkOuter, 0); 
        
        if (i==0)
           showLabelValue((int )knobStartValue,(knobRadius+knobOffsetMarks+knobLengthMarkOuter+knobValueLabelOffset));
        else   
           showLabelValue((int )knobEndValue,(knobRadius+knobOffsetMarks+knobLengthMarkOuter+knobValueLabelOffset));
      }
      else if (i<(knobSteps-5))
      {
        applet.stroke(internalColorMarks);
        if (knobPosition==i)
          applet.stroke(colorMarksLight);

        if ((i%knobSkips)==0)
        {
          if ((int)knobPosition==i)
            applet.strokeWeight(strokeOutterMarks+knobPlusStroke);
          else
             applet.strokeWeight(strokeOutterMarks);
     
          applet.line(knobRadius+knobOffsetMarks, 0, knobRadius+knobOffsetMarks+knobLengthMarkInner, 0);
          showLabelValue((int )ksv+1,(knobRadius+knobOffsetMarks+knobLengthMarkInner+knobValueLabelOffset));
          
        }
        ksv+=knobSum;
        
      }
      applet.popMatrix();
    }
    applet.popMatrix(); 
  }
////////////////////////////////////////////////////////////////////////
  void drawIndicator()
  {

    applet.pushMatrix();
    applet.translate(knobX,knobY);
    
    //Initial Position
    applet.rotate(knobIndicatorOffset+angle);

    applet.stroke(colorOutterIndicator);
    applet.strokeWeight(strokeOutterIndicator);
    applet.line(outterOffsetFromCenter,0,knobOutterLength,0);

    applet.stroke(colorInnerIndicator);
    if (mouseOver(false)) applet.stroke(konbInnerIndicatorHigh);
    applet.strokeWeight(strokeInnerIndicator);
    applet.line(innerOffsetFromCenter,0,knobInnerLength,0);
    applet.popMatrix(); 
  }
////////////////////////////////////////////////////////////////////////
  void drawCircle()
  {
    applet.pushMatrix();
    applet.translate(0,0);  
    applet.stroke(colorExternalCircle);
    applet.fill(internalColor);
    applet.strokeWeight(strokeExternalCircle);
    applet.ellipse(knobX,knobY,knobRadius*2,knobRadius*2);
    applet.popMatrix();
  }
////////////////////////////////////////////////////////////////////////
  public float update()
  {

    if (mouseOver(false) && applet.keyPressed && !knobKeyPressed)
    {
      knobKeyPressed=true;
       ///have to be from one on ones
       if (applet.keyCode==PApplet.DOWN || applet.keyCode==PApplet.LEFT) for (int i=0; i<(int )(knobSteps/10); i++) decrementKnobPosition();
       if (applet.keyCode==PApplet.UP || applet.keyCode==PApplet.RIGHT) for (int i=0; i<(int )(knobSteps/10); i++) incrementKnobPosition();
    }
    if (!applet.keyPressed) knobKeyPressed=false;
    
    drawCircle(); 
    drawIndicator();
    drawValues();
    drawLabels();
    drawExternalMarks();
    change();

    
    return knobValue;
  }
////////////////////////////////////////////////////////////////////////
  void deBounce(int n)
  {
    if (pressOnlyOnce) 
      return;
    else
      
    if (deb++ > n) 
    {
      deb=0;
      pressOnlyOnce=true;
    }
    
  }    
////////////////////////////////////////////////////////////////////////
  boolean mouseOver(boolean clicked)
  {
    if ((applet.mouseX > (knobX-knobRadius-knobOutterLength) && applet.mouseX < (knobX+knobRadius+knobOutterLength)) && (applet.mouseY > (knobY-knobRadius-knobOutterLength) && applet.mouseY < (knobY+knobRadius+knobOutterLength)))
    {
      if (applet.mouseButton==PApplet.LEFT && clicked)
         knobmouseOver=true;

      if (!clicked)
         knobmouseOver=true;

      if (applet.mousePressed && applet.mouseButton==PApplet.LEFT && applet.keyPressed && debug)
      {
        if (applet.keyCode==PApplet.CONTROL)
        {
          knobX=knobX+(int )((float )(applet.mouseX-applet.pmouseX)*0.4);
          knobY=knobY+(int )((float )(applet.mouseY-applet.pmouseY)*0.4);
          knobMoving=true;
          System.out.println("Moving...");
        }
        if (applet.keyCode==PApplet.SHIFT && pressOnlyOnce) 
        {
          printGeometry();
          pressOnlyOnce=false;
        }
        deBounce(5);
      }
      
      if (!applet.mousePressed) knobMoving=false;

    }
    else
      knobmouseOver=false;

    return knobmouseOver;
  }
////////////////////////////////////////////////////////////////////////
  int delta()
  {
    float offx = applet.mouseX - knobX;
    float offy = applet.mouseY - knobY;
     
    float thisAngle = PApplet.atan2(offy, offx);
    float prevAngle = PApplet.atan2(applet.pmouseY - knobY, applet.pmouseX - knobX);
    
    float diff = thisAngle - prevAngle;
   
    return (thisAngle < prevAngle) ? -1 : 1;    
  }
////////////////////////////////////////////////////////////////////////
  void change()
  {
    if (!mouseOver(true) || knobMoving) return;
    
    if (applet.mousePressed && applet.mouseButton==PApplet.LEFT)
    {
    
      float thisAngle = PApplet.atan2(applet.mouseY - knobY, applet.mouseX - knobX);
  
      float steps = knobTotalAngle/knobSteps;
    
      if (thisAngle<=0 || (thisAngle>0 && thisAngle<PApplet.PI/2)) thisAngle=thisAngle+PApplet.TWO_PI;
    
    
        for (int i = 0; i < knobSteps; i++)
        {
          if (thisAngle >= knobStops[i] && thisAngle <  knobStops[i]+steps)
            if (thisAngle >= (knobStops[i]+(steps/2)))
            { 
              updateKnobPosition(i+1);
              i=knobSteps;
             }  
             else
            {
              updateKnobPosition(i);
              i=knobSteps;
            }
        }
   
    
      //Contraint to min,max
      if (thisAngle > knobMaxEndAngle) thisAngle=knobMaxEndAngle; 
      if (thisAngle < knobMaxIniAngle) thisAngle=knobMaxIniAngle;
    
      knobActualAngle=thisAngle;
      
  }  
  
    return;    
  }
////////////////////////////////////////////////////////////////////////
  public float getKnobValue()
  {
    return knobValue;
  }
////////////////////////////////////////////////////////////////////////
  int getKnobPosition()
  {
    return knobPosition;
  }
////////////////////////////////////////////////////////////////////////
  public void setColor(int kcolor)
  {
    internalColor=kcolor;
    internalColorLight=internalColor+0xFF230F0F;
  }
////////////////////////////////////////////////////////////////////////
  void updateKnobPosition(int position)
  {
    if (position>knobSteps) position=knobSteps;
    if (position<0) position=0;
    
    prevKnobPosition=knobPosition;
    knobPosition=position;
    
    if (knobSnap)
       angle=knobStops[position];
    else
       angle=knobActualAngle;      
      
  }
////////////////////////////////////////////////////////////////////////
  void setKnobPosition(int position)
  {
    if (position>knobSteps) position=knobSteps;
    if (position<0) position=0;
    
    prevKnobPosition=knobPosition;
    knobPosition=position;
    angle=knobStops[position];
      
  }
////////////////////////////////////////////////////////////////////////
  public void setKnobValue(float value)
  {
    if (value<knobStartValue) value=knobStartValue;
    if (value>knobEndValue) value=knobEndValue;
   
    setKnobPosition((int )(knobSteps*(knobStartValue-value)/(knobStartValue-knobEndValue)));
      
  }
////////////////////////////////////////////////////////////////////////
  float getAnglefromPosition()
  {
     return knobStops[knobPosition];
  }
////////////////////////////////////////////////////////////////////////
  public void changeKnobPositionWithWheel(int delta)
  {
    if (!mouseOver(false)) return;
    
    if (applet.keyPressed && applet.keyCode==PApplet.SHIFT) delta=delta*(int )(knobSteps/10);
    if (applet.keyPressed && applet.keyCode==PApplet.CONTROL) delta=delta*(int )(knobSteps/4);    
    
    prevKnobPosition=knobPosition;
    knobPosition+=delta;
    if (knobPosition<0) knobPosition=0;
    
    if (knobPosition>knobSteps) knobPosition=knobSteps;

    angle=knobStops[knobPosition];

  }
////////////////////////////////////////////////////////////////////////
  void incrementKnobPosition()
  {
    prevKnobPosition=knobPosition;
    knobPosition++;
    if (knobPosition>knobSteps) knobPosition=knobSteps;

    if (knobSnap)
       angle=knobStops[knobPosition];
    else   
       angle=knobStops[knobPosition];
  } 
////////////////////////////////////////////////////////////////////////
  void decrementKnobPosition()
  {
    prevKnobPosition=knobPosition;
    knobPosition--;
    if (knobPosition<0) knobPosition=0;

    if (knobSnap)
       angle=knobStops[knobPosition];
    else   
       angle=knobStops[knobPosition];
  } 
////////////////////////////////////////////////////////////////////////
  void setKnobRadius(int r)
  {
    knobRadius=r;
    update();
  }
////////////////////////////////////////////////////////////////////////
  void setWidthExternalCircle(int w)
  {
    strokeExternalCircle=w;
  }
////////////////////////////////////////////////////////////////////////
  void setOutterOffsetFromCenter(int o)
  {
    outterOffsetFromCenter=o;
  }
////////////////////////////////////////////////////////////////////////
  void setKnobOutterLength(int o)
  {
    knobOutterLength=o;
  }
////////////////////////////////////////////////////////////////////////
  void setInnerOffsetFromCenter(int o)
  {
    innerOffsetFromCenter=o;
  }
////////////////////////////////////////////////////////////////////////
  void setKnobInnerLength(int o)
  {
    knobInnerLength=o;
  }
////////////////////////////////////////////////////////////////////////
  void setKnobLengthMarkOuter(int o)
  {
    knobLengthMarkOuter=o;
  }
////////////////////////////////////////////////////////////////////////
  void setKnobLengthMarkInner(int o)
  {
    knobLengthMarkInner=o;
  }
////////////////////////////////////////////////////////////////////////
  void setMarkAngle(int o)
  {
    knobRightAngle=o;
    knobMaxIniAngle=(180-knobRightAngle)*PApplet.PI/180;
    knobMaxEndAngle=knobRightAngle*PApplet.PI/180+PApplet.TWO_PI;
    knobTotalAngle=2*knobRightAngle*PApplet.PI/180+PApplet.PI;
    calculateStops();
    setKnobPosition(knobPosition);
  }
////////////////////////////////////////////////////////////////////////
  void setKnobLabelTextSize(int o)
  {
    knobLabelTextSize=o;
  }
////////////////////////////////////////////////////////////////////////
  void setKnobLabelOffset(int o)
  {
    knobLabelOffset=o;
  }
////////////////////////////////////////////////////////////////////////
  void setKnobValueTextSize(int o)
  {
    knobValueTextSize=o;
  }
////////////////////////////////////////////////////////////////////////
  void setKnobValueOffset(int o)
  {
    knobValueOffset=o;
  }
////////////////////////////////////////////////////////////////////////
  void setKnobIndicatorOffset(float o)
  {
    knobIndicatorOffset=o;
  }
////////////////////////////////////////////////////////////////////////
  void setKnobOffsetMarks(int o)
  {
    knobOffsetMarks=o;
  }
////////////////////////////////////////////////////////////////////////
  void setStrokeOutterMarks(int o)
  {
    strokeOutterMarks=o;
  }
////////////////////////////////////////////////////////////////////////
  void setStrokeOutterInitEndMarks(int o)
  {
    strokeOutterInitEndMarks=o;
  }
////////////////////////////////////////////////////////////////////////
  void setStrokeOutterIndicator(int o)
  {
    strokeOutterIndicator=o;
  }
////////////////////////////////////////////////////////////////////////
  void setStrokeInnerIndicator(int o)
  {
    strokeInnerIndicator=o;
  }
////////////////////////////////////////////////////////////////////////
  void setShowKnobLabelValues(boolean o)
  {
    showKnobLabelValues=o;
  }  
////////////////////////////////////////////////////////////////////////
  boolean getShowKnobLabelValues()
  {
    return showKnobLabelValues;
  }  
////////////////////////////////////////////////////////////////////////
  void setColorExternalCircle(int o)
  {
    colorExternalCircle=o;
  }
////////////////////////////////////////////////////////////////////////
  int getColorExternalCircle()
  {
    return colorExternalCircle;
  }
////////////////////////////////////////////////////////////////////////
  void setInternalColor(int o)
  {
    internalColor=o;
  }
////////////////////////////////////////////////////////////////////////
  int getInternalColor()
  {
    return internalColor;
  }
////////////////////////////////////////////////////////////////////////
  void setColorOutterIndicator(int o)
  {
    colorOutterIndicator=o;
  }
////////////////////////////////////////////////////////////////////////
  int getColorOutterIndicator()
  {
    return colorOutterIndicator;
  }
////////////////////////////////////////////////////////////////////////
  void setColorInnerIndicator(int o)
  {
    colorInnerIndicator=o;
  }
////////////////////////////////////////////////////////////////////////
  int getColorInnerIndicator()
  {
    return colorInnerIndicator;
  }
////////////////////////////////////////////////////////////////////////
  void setInternalColorMarks(int o)
  {
    internalColorMarks=o;
  }
////////////////////////////////////////////////////////////////////////
  int getInternalColorMarks()
  {
    return internalColorMarks;
  }
////////////////////////////////////////////////////////////////////////
  void setExternalColorMarks(int o)
  {
    externalColorMarks=o;
  }
////////////////////////////////////////////////////////////////////////
  int getExternalColorMarks()
  {
    return externalColorMarks;
  }
////////////////////////////////////////////////////////////////////////
  void setKnobColorValueLabel(int o)
  {
    knobColorValueLabel=o;
  }
////////////////////////////////////////////////////////////////////////
  int getKnobColorValueLabel()
  {
    return knobColorValueLabel;
  }
////////////////////////////////////////////////////////////////////////
  void setKnobColorLabelText(int o)
  {
    knobColorLabelText=o;
  }
////////////////////////////////////////////////////////////////////////
  int getKnobColorLabelText()
  {
    return knobColorLabelText;
  }
////////////////////////////////////////////////////////////////////////
  void setKnobColorValueText(int o)
  {
    knobColorValueText=o;
  }
////////////////////////////////////////////////////////////////////////
  int getKnobColorValueText()
  {
    return knobColorValueText;
  }
////////////////////////////////////////////////////////////////////////
  void setKnobValueLabelSize(int o)
  {
    knobValuelabelSize=o;
  }
////////////////////////////////////////////////////////////////////////
  int getKnobValueLabelSize()
  {
    return knobValuelabelSize;
  }
////////////////////////////////////////////////////////////////////////
  void setKnobValueLabelOffset(int o)
  {
    valueLabelOffset=o;
  }
////////////////////////////////////////////////////////////////////////
  int getKnobValueLabelOffset()
  {
    return valueLabelOffset;
  }
////////////////////////////////////////////////////////////////////////
  void showKnobLabelText()
  {
    knobLabelTextVisible=true;
  }
////////////////////////////////////////////////////////////////////////
  void hideKnobLabelText()
  {
    knobLabelTextVisible=false;
  }
////////////////////////////////////////////////////////////////////////
  int getColorRGB(int val, int pos)
  {
    int rvalue=0;
    //B 
    if(pos==3) rvalue=(int )applet.blue(val);
    //G
    if(pos==2) rvalue=(int )applet.green(val);
    //R
    if(pos==1) rvalue=(int )applet.red(val);
    return rvalue;
    
  }
////////////////////////////////////////////////////////////////////////
  void setKnobNumberOfMarks(int o)
  {
    if (knobSteps>=o)
    {
      knobNumberOfMarks=o;
      knobSkips=knobSteps/knobNumberOfMarks;
    }
  }
////////////////////////////////////////////////////////////////////////
  void setColors(int ic, int ec, int ii, int oi, int icm, int ecm, int cvl, int clt, int cvt)
  {
    internalColor=ic;
    colorExternalCircle=ec;
    colorInnerIndicator=ii;
    colorOutterIndicator=oi;
    internalColorMarks=icm;
    externalColorMarks=ecm;
    knobColorValueLabel=cvl;
    knobColorLabelText=clt;
    knobColorValueText=cvt;
  }
////////////////////////////////////////////////////////////////////////
  void getKnobParameters()
  {

    System.out.println("ADknob knob;");
    System.out.println("void setup()");
    System.out.println("{");
    System.out.println("  size(300,300);");
    System.out.println("  smooth();");
    System.out.print("  knob = new ADknob(");
    System.out.print("\"KNOB\","+PApplet.str(150)+","+PApplet.str(150)+","+PApplet.str(knobRadius)+","+PApplet.str(knobSteps)+","+PApplet.str(knobStartValue)+","+PApplet.str(knobSum)+",");
    System.out.print(PApplet.str(strokeExternalCircle)+","+PApplet.str(outterOffsetFromCenter)+","+PApplet.str(knobOutterLength)+",");
    System.out.print(PApplet.str(innerOffsetFromCenter)+","+PApplet.str(knobInnerLength)+","+PApplet.str(knobLengthMarkOuter)+","+PApplet.str(knobLengthMarkInner)+",");
    System.out.print(PApplet.str(knobRightAngle)+","+PApplet.str(knobLabelTextSize)+","+PApplet.str(knobLabelOffset)+","+PApplet.str(knobValueTextSize)+","+PApplet.str(knobValueOffset)+",");
    System.out.print(PApplet.str(knobOffsetMarks)+","+PApplet.str(strokeOutterMarks)+","+PApplet.str(strokeOutterInitEndMarks)+","+PApplet.str(strokeOutterIndicator)+","+PApplet.str(strokeInnerIndicator)+","+PApplet.str(knobNumberOfMarks)+",");
    System.out.print(PApplet.str(showKnobLabelValues)+","+PApplet.str(knobValuelabelSize)+","+PApplet.str(valueLabelOffset));
    System.out.println(");");

    System.out.print("  knob.setColors(");
    System.out.print(PApplet.str(internalColor)+","+PApplet.str(colorExternalCircle)+","+PApplet.str(colorInnerIndicator)+","+PApplet.str(colorOutterIndicator)+",");
    System.out.print(PApplet.str(internalColorMarks)+","+PApplet.str(externalColorMarks)+","+PApplet.str(knobColorValueLabel)+","+PApplet.str(knobColorLabelText)+","+PApplet.str(knobColorValueText));   
    System.out.println(");");
    System.out.println("  activateMouseWheel();");
    System.out.println("}");
    
    System.out.println("\nvoid draw()");
    System.out.println("{");    
    System.out.println("  background(#E0E0E0);");
    System.out.println("  knob.update();");
    System.out.println("}");
    
    System.out.println("\nvoid activateMouseWheel()");
    System.out.println("{");
    System.out.println(" addMouseWheelListener(new java.awt.event.MouseWheelListener()"); 
    System.out.println(" {"); 
    System.out.println("   public void mouseWheelMoved(java.awt.event.MouseWheelEvent evt)"); 
    System.out.println("   { ");
    System.out.println("     mouseWheel(evt.getWheelRotation());");
    System.out.println("   }");
    System.out.println(" });");

    System.out.println("}");
        
    System.out.println("\nvoid mouseDragged()");
    System.out.println("{");
    System.out.println("  knob.change();");
    System.out.println("}");    
    
    System.out.println("\nvoid mouseWheel(int delta)");
    System.out.println("{");
    System.out.println("  knob.changeKnobPositionWithWheel(delta);");
    System.out.println("}");     
  }
////////////////////////////////////////////////////////////////////////
  void printGeometry()
  {
	System.out.print("  knob = new ADknob(");
    System.out.println("\""+knobLabelText+"\","+PApplet.str(knobX)+","+PApplet.str(knobY)+","+PApplet.str(knobRadius)+","+PApplet.str(knobSteps)+","+PApplet.str(knobStartValue)+","+PApplet.str(knobSum)+");");
  }
////////////////////////////////////////////////////////////////////////
  void setDebugOn()
  {
    debug=true;
  }
////////////////////////////////////////////////////////////////////////
  void setDebugOff()
  {
    debug=false;
  }  
} //end Class Knob
