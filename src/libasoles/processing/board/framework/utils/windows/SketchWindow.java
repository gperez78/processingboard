/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package libasoles.processing.board.framework.utils.windows;

import libasoles.processing.board.framework.sketches.SketchManager;
import processing.event.KeyEvent;
import processing.event.MouseEvent;


public class SketchWindow extends Window{
        
    public SketchManager sm;
    
    public SketchWindow() {
        super();        
        sm = new SketchManager();
    }
    public SketchWindow(SketchManager sm) {
        super();
        this.sm = sm;
    }
    public SketchWindow( String renderer ) {
        super(renderer);
    }
    public SketchWindow( String renderer, boolean fullscreen  ) {
    	super(renderer, fullscreen );    	
    	sm = new SketchManager();
    }
    
    public void setSketchManager(SketchManager sm) {
        this.sm = sm;
    }
    public SketchManager getSketchManager() {
    	return sm;
    }
    
    @Override
    public void setup() {
    	
    }   
    
    @Override
    public void draw() {
                
        sm.draw();
    }   
    
    @Override
	public void keyPressed(KeyEvent event) {
    	super.keyPressed(event);
		sm.keyPressed(event);
	}
	@Override
	public void keyReleased(KeyEvent event) {
		super.keyReleased(event);
		sm.keyReleased(event);
	}
	@Override
	public void keyTyped(KeyEvent event) {
		super.keyTyped(event);
		sm.keyTyped(event);
	}
	@Override
	public void mouseClicked(MouseEvent event) {
		super.mouseClicked(event);
		sm.mouseClicked(event);
	}
	@Override
	public void mouseDragged(MouseEvent event) {
		super.mouseDragged(event);
		sm.mouseDragged(event);
	}
	@Override
	public void mouseEntered(MouseEvent event) {
		super.mouseEntered(event);
		sm.mouseEntered(event);
	}
	@Override
	public void mouseExited(MouseEvent event) {
		super.mouseExited(event);
		sm.mouseExited(event);
	}
	@Override
	public void mouseMoved(MouseEvent event) {
		super.mouseMoved(event);
		sm.mouseMoved(event);
	}
	@Override
	public void mousePressed(MouseEvent event) {
		super.mousePressed(event);		
		sm.mousePressed(event);
	}
	@Override
	public void mouseReleased(MouseEvent event) {
		super.mouseReleased(event);
		sm.mouseReleased(event);
	}
	@Override
	public void mouseWheel(MouseEvent event) {
		super.mouseWheel(event);
		sm.mouseWheel(event);
	}
}
