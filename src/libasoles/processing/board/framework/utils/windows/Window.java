package libasoles.processing.board.framework.utils.windows;

import processing.core.PApplet;
import processing.core.PImage;
import processing.event.KeyEvent;
import processing.event.MouseEvent;

public class Window extends PApplet {

	public String name = "window";
	
    String renderer = P2D;
    boolean fullscreen;
    public static PImage mirror;
    
    public String getName(){
    	return name;
    }
    
    public Window( ) {
        super();
    }
    public Window( String renderer ) {
        super();
        this.renderer = renderer;
    }
    public Window( String renderer, boolean fullscreen ) {
        super();
        this.fullscreen = fullscreen;
        this.renderer = renderer;
    }

    @Override
    public void settings() {
    	
    	if(fullscreen)
    		fullScreen();
    	
        size(800, 600, renderer); //This is causing OpenGL errors. Why?
        //size(800, 600);
    }

    @Override
    public void setup() {

    }

    @Override
    public void draw() {

    }
    
	@Override
	public void keyPressed(KeyEvent event) {
		super.keyPressed(event);
	}
	@Override
	public void keyReleased(KeyEvent event) {
		super.keyReleased(event);
	}
	@Override
	public void keyTyped(KeyEvent event) {
		super.keyTyped(event);
	}
	@Override
	public void mouseClicked(MouseEvent event) {
		super.mouseClicked(event);
	}
	@Override
	public void mouseDragged(MouseEvent event) {
		super.mouseDragged(event);
	}
	@Override
	public void mouseEntered(MouseEvent event) {
		super.mouseEntered(event);
	}
	@Override
	public void mouseExited(MouseEvent event) {
		super.mouseExited(event);
	}
	@Override
	public void mouseMoved(MouseEvent event) {
		super.mouseMoved(event);
	}
	@Override
	public void mousePressed(MouseEvent event) {
		super.mousePressed(event);
	}
	@Override
	public void mouseReleased(MouseEvent event) {
		super.mouseReleased(event);
	}
	@Override
	public void mouseWheel(MouseEvent event) {
		super.mouseWheel(event);
	}
    
}
