package libasoles.processing.board.framework.utils.windows;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Hashtable;

import libasoles.processing.board.framework.sketches.Sketch;
import libasoles.processing.board.framework.utils.ADknob;
import libasoles.processing.board.framework.utils.Radio;
import libasoles.processing.board.framework.utils.windows.SketchWindow;
import processing.core.PFont;
import processing.core.PImage;
import processing.core.PVector;
import processing.event.KeyEvent;
import processing.event.MouseEvent;

public class ControlWindow extends Window {

	public String name = "control window";

	//Window mainWindow;
	private PImage sketchWindowImage;

	ArrayList<Sketch> sketches;    
	SketchWindow sketchWindow;

	public String savePath; // default output folder for saveImage()
	
	public boolean inited = false;
	
	PFont textFont;
	
	Radio sketchesList;
	PVector sketchListPos = new PVector(830, 20); 
	//PVector sketchListSize = new PVector(840, 50); // not needed
	
	PVector videoPadPos = new PVector(0, 0);
	PVector videoPadSize = new PVector(800, 600);

	PVector descriptionPos = new PVector(820, 450);
	PVector descriptionSize = new PVector(260, 150);

	// controls
	PVector controlsAreaPos = new PVector(10, 610);
	PVector controlsAreaSize = new PVector(790, 175);
	
	PVector BasicCommandsAreaPos = new PVector(820, 610);
	PVector BasicCommandsAreaSize = new PVector(260, 140);
	
	Hashtable<String, ADknob> sketchControls = new Hashtable<String, ADknob>();

	private String basicCommandsDescription = ""
			+ "p = pause\n"
			+ "space = next sketch\n"
			+ "s = save a snapshot";

	private boolean burst;

	private int lastStatusTime;
	private int lastSnapshotTime;
	
	private String statusText = "";
	
	public ControlWindow() {
		super();
	}

	public ControlWindow(SketchWindow sw) {

		super();
		sketchWindow = sw;
		sketchWindowImage = new PImage(); 		
	}

	public ControlWindow(String renderer) {
		super(renderer);
	}

	@Override
	public void settings() {
		//super.settings();
		//fullScreen(1);
		size(1100, 800); 
	}

	@Override
	public void setup() {
		super.setup();    

        savePath = sketchPath("");		
        
        textFont = createFont("Arial", 16, true);      
        textFont(textFont, 16); 
	}
	
	public void init(){
		inited = true;
		System.out.println("Control window running.");
	}
	
	public void listSketches(){

		sketches = sketchWindow.sm.getAll();

		String[] list = new String[sketches.size()];

		int i = 0;
		for(Sketch s : sketches){

			list[i] = s.getName();     
			i++;
		};   

		sketchesList = new Radio(this, (int)sketchListPos.x, (int)sketchListPos.y, list, "radioButton"); 
		sketchesList.setCallback( "update" );
		sketchesList.setDebugOn();
		sketchesList.setBoxFillColor(255);  
		sketchesList.setValue( (sketchWindow.sm.getCurrentSketch().id) ); // mark current sketch		
	}
	
	public void addControl(String name, int range, float initialValue){
		
		addControl("knob", name, range, initialValue, -1);
	}

	public void addControl(String group, String name, int range, float initialValue){
		
		addControl(group, name, range, initialValue, -1);
	}

	public void addControl(String group, String name, int range, float initialValue, int knobColor){
		
		int r = 20;
		int paddingX = 28;
		int paddingY = 40;
		int slots = (int) controlsAreaSize.x / ((r + paddingX)*2); 
		
		int existingControls = sketchControls.size();
		
		int xPos = (int) controlsAreaPos.x + 60;
		int yPos = (int) controlsAreaPos.y + 50;
		if(existingControls >= slots){
			yPos+= r*2 + paddingY;
			xPos+=(r + paddingX)*2*(existingControls-slots);
		} else {
			xPos+=(r + paddingX)*2*existingControls;
		}			
		
		PVector pos = new PVector(xPos, yPos);
		
		ADknob knob = new ADknob(this, name, (int)pos.x, (int)pos.y, 20, range, 0, 1, // name, x, y, r, range, extra range, stops, increase by
				2.0f, 0.0f, 14.0f, 0.0f, 34.0f, 10, 1, 24.0f, 11, 19, 9, 10, 10, 2, 5, 10, 4, 10, false, 11, 0); // the last 10 is quantity of dots
		
		knob.setGroup(group);
				
		if(knobColor != -1){			
			knob.setColor(knobColor);
		} else if(range == 1){
			knob.setColor(0xFF06648D);
		} else {
			knob.setColor(0xFF11BF55);
		}
		
		knob.setKnobValue(initialValue);	
		
		sketchControls.put( group + "-" + name, knob );
	}
	
	public float getControlValue(String name){
		
		return getControlValue("knob", name);		
	}
	
	public float getControlValue(String group, String name){
		
		if(sketchControls.containsKey(group + "-" + name))
			return sketchControls.get(group + "-" + name).getKnobValue();
		
		return 0;		
	}
	
	public void clearControlKnobs(){
		
		sketchControls.clear();
	}
	
	public void clearControlKnobsGroup(String group){
		
		if(sketchControls.isEmpty())
			return;
		
		Enumeration<String> enumKey = sketchControls.keys();
		while(enumKey.hasMoreElements()) {
		    String name = enumKey.nextElement();
		    ADknob knob = sketchControls.get(group + "-" + name);
		    if(knob.getGroup() == group)
		    	sketchControls.remove(key); // TODO: this should work
		}
	}
	
	public void hideControlKnobGroup(String group){
		for(String key : sketchControls.keySet()){
			if(sketchControls.get(key).getGroup() == group)
				sketchControls.get(key).hide();	
		}
	}
	
	public void showControlKnobGroup(String group){
		for(String key : sketchControls.keySet()){
			if(sketchControls.get(key).getGroup() == group)
				sketchControls.get(key).show();	
		}
	}
	
	public void check(int sketchID){

		sketchesList.setValue( sketchID );
	}

	@Override
	public void draw() {

		super.draw();
				
		if(inited == false){
			
			drawLoading();
			return;
		}			
				
		background(200);
		
		sketchesList.update();
		    
        textFont(textFont, 16); 
		
		noStroke();
		
		drawVideoPad();
		
		drawDescription();
		
		drawControlArea();	
		
		drawBasicCommandsArea();
		
		drawStatusLight();
		
		drawStatus();
		
		if(burst && millis() > lastSnapshotTime+3000){
			burst();
		}			
	}

	/* callback to ui radio buttons */
	public boolean update(int n) {  

		boolean switching = sketchWindow.sm.setCurrentSketch(n);
		if(switching){
			println("switching to " + sketchWindow.sm.getCurrentSketch().getName()); 
			return true;
		}
		println("ERROR switching to " + sketchWindow.sm.getCurrentSketch().getName()); 
		return false;
	}
	
	public void drawLoading(){
		
		background(0);
		
		pushMatrix();
		fill(178,178,126);
		
		translate((width/2)-30, (height/2)-55);
		
		if(millis()%5000 < 1000){
			
			float radians = radians(map((millis()%1000), 0, 1000, 0, 180));
			rotate( sin(radians) );						
		}
		
		rect(-23, -23, 46, 46);
		popMatrix();

		textSize(26);
		textAlign(CENTER);
		text("loading...", width/2, height/2);
	}

	public void drawVideoPad(){

		// video pad
		fill(255);
		rect(videoPadPos.x, videoPadPos.y, videoPadPos.x + videoPadSize.x, videoPadPos.y + videoPadSize.y);

		// main window content
		sketchWindowImage = Window.mirror;
		image(sketchWindowImage, videoPadPos.x, videoPadPos.y, videoPadSize.x, videoPadSize.y);
	}
	
	public boolean eventIsInsideVideoPad(MouseEvent e){
		
		if(e.getX() >= videoPadPos.x && e.getX() <= videoPadPos.x + videoPadSize.x && e.getY() >= videoPadPos.y && e.getY() <= videoPadPos.y + videoPadSize.y) 
			return true;
		return false;
	}
	
	private boolean eventIsInsideControlArea(MouseEvent e) {
		
		if(e.getX() >= controlsAreaPos.x && e.getX() <= controlsAreaPos.x + controlsAreaSize.x && e.getY() >= controlsAreaPos.y && e.getY() <= controlsAreaPos.y + controlsAreaSize.y) 
			return true;
		return false;
	}
	
	public void drawDescription(){

		// video pad
		fill(255);
		rect(descriptionPos.x, descriptionPos.y, descriptionSize.x, descriptionSize.y);
		
		fill(0);
		int padding = 5;		
		text(sketchWindow.sm.getCurrentSketch().description, descriptionPos.x+padding*2, descriptionPos.y+padding, descriptionSize.x-padding*2, descriptionSize.y-padding);		
	}
	
	public void drawControlArea(){
		
		  fill(255);  
		  strokeWeight(1);
		  stroke(200);
		  rect(controlsAreaPos.x, controlsAreaPos.y, controlsAreaSize.x, controlsAreaSize.y);
		  
		  Enumeration<String> controls = sketchControls.keys();
		  while( controls.hasMoreElements() )
	      {
		      Object key = controls.nextElement();
		      ADknob knob = sketchControls.get(key);
		      if(knob.isVisible())
		    	  knob.update();
	      }
	}
	
	public void drawBasicCommandsArea(){
		
		rectMode(CORNER);
		textAlign(CORNER);
		textSize(14);
		
		fill(255);  
		strokeWeight(1);
		stroke(200);
		rect(BasicCommandsAreaPos.x, BasicCommandsAreaPos.y, BasicCommandsAreaSize.x, BasicCommandsAreaSize.y);
		
		fill(0);
		int padding = 5;		
		text(basicCommandsDescription, BasicCommandsAreaPos.x+padding*2, BasicCommandsAreaPos.y+padding, BasicCommandsAreaSize.x-padding*2, BasicCommandsAreaSize.y-padding);		
	}
		
	public void drawStatus(){
		
		fill(0);		
		text(statusText, width - BasicCommandsAreaSize.x -10, height - 35, BasicCommandsAreaSize.x, 40);
		
		if(millis() > lastStatusTime+7000){
			
			// reset status text
			statusText = "";
			lastStatusTime = millis(); 
		}
	}
	
	public void drawStatus(String text){
		
		fill(0);
		statusText = text;
		drawStatus();
	}

	public void drawStatusLight(int color){
		
		fill(color);		
		ellipse(width - 25, height - 25, 15, 15);
	}
	
	public void drawStatusLight(){

		int color;
		
		// status 
		if(sketchWindow.sm != null && sketchWindow.sm.getCurrentSketch().isRunning()){
			color=color(0, 255, 0);
		} else {
			color=color(255, 255, 0);
		}
		
		drawStatusLight(color);
	}
	
	public void takeSnapshot(){
		
		drawStatusLight( color(199, 21, 133) ); 
		PImage screen = get((int)videoPadPos.x, (int)videoPadPos.y, (int)(videoPadPos.x + videoPadSize.x), (int)(videoPadPos.y + videoPadSize.y));
		String picName = sketchWindow.sm.getCurrentSketch().getName();
	    screen.save(savePath + "/captures/"+ picName + "-" + (millis()%10000) + ".jpg");
	}
	
	public void burst(){	
		drawStatus("Burst mode");
		takeSnapshot();
		lastSnapshotTime = millis();
	}
	
	public void keyPressed(KeyEvent event) {

		if(sketchWindow.sm.getCurrentSketch() != null){

			Sketch s = sketchWindow.sm.getCurrentSketch();

			switch(event.getKey()){		
			case ' ':
				try{
					
					int currentSketchIndex = s.id;
					int nextSketchIndex = currentSketchIndex+1;
					if(nextSketchIndex >= sketches.size())
						nextSketchIndex = 0; // loop
					
					if (sketchWindow.sm.setCurrentSketch( nextSketchIndex ))
						sketchesList.setValue( nextSketchIndex ); // we index from 0 in controlBox
				} catch(Exception e){
		    		System.out.println(e.getMessage());
		    	}
				break;					
			case 'p':
			case 'P':
				if(s.isRunning())
					s.pause();
				else
					s.resume();
				break;
			case 'S':					
					burst = !burst; // snapshots burst
					if(!burst)
						drawStatus(" ");
				break;
			case 's':				
					// take a snapshot
					takeSnapshot();
				break;
			default:
				sketchWindow.sm.keyPressed(event);
				break;					
			}				
		}
	}
	
	public void keyReleased(KeyEvent event) {

		sketchWindow.sm.keyReleased(event);		
	}
	
	public void keyTyped(KeyEvent event) {
		
		sketchWindow.sm.keyTyped(event);		
	}
	
	public void mouseClicked(MouseEvent event) {
		if(eventIsInsideVideoPad(event))
			sketchWindow.sm.mouseClicked(event);
	}

	public void mouseDragged(MouseEvent event) {
		if(eventIsInsideVideoPad(event))
			sketchWindow.sm.mouseDragged(event);
	}

	public void mouseEntered(MouseEvent event) {
		if(eventIsInsideVideoPad(event))
			sketchWindow.sm.mouseEntered(event);
	}

	public void mouseExited(MouseEvent event) {
		if(eventIsInsideVideoPad(event))
			sketchWindow.sm.mouseExited(event);
	}

	public void mouseMoved(MouseEvent event) {
		if(eventIsInsideVideoPad(event))
			sketchWindow.sm.mouseMoved(event);
	}

	public void mousePressed(MouseEvent event) {
		if(eventIsInsideVideoPad(event))
			sketchWindow.sm.mousePressed(event);
	}

	public void mouseReleased(MouseEvent event) {
		if(eventIsInsideVideoPad(event))
			sketchWindow.sm.mouseReleased(event);
	}

	public void mouseWheel(MouseEvent event) {
		if(eventIsInsideVideoPad(event))
			sketchWindow.sm.mouseWheel(event);
		if(eventIsInsideControlArea(event)){

		  Enumeration<String> controls = sketchControls.keys();
		  while( controls.hasMoreElements() )
	      {
		      Object key = controls.nextElement();
		      ADknob knob = sketchControls.get(key);
		      knob.changeKnobPositionWithWheel(event.getCount());
	      }
		}			
	}
}
