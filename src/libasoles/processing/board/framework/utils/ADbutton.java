package libasoles.processing.board.framework.utils;

import libasoles.processing.board.framework.utils.windows.Window;
import processing.core.PApplet;

/*
 * ----------------------------------
 *  Button Class for Processing 2.0
 * ----------------------------------
 *
 * this is a simple button class. The following shows 
 * you how to use it in a minimalistic way.
 *
 * DEPENDENCIES:
 *   N/A
 *
 * Created:  Mapplet.arch, 16 2012
 * Author:   Alejandro Dirgan
 * Version:  1.2
 *
 * License:  GPLv3
 *   (http://www.fsf.org/licensing/)
 *
 * Follow Us
 *    adirgan.blogspot.com
 *    twitter: @ydirgan
 *    https://www.facebook.com/groups/mmiiccrrooss/
 *    https://plus.google.com/b/111940495387297822358/
 *
 * DISCLAIMER **
 * THIS SOFTWARE IS PROVIDED TO YOU "AS IS," AND WE MAKE NO EXPRESS OR IMPLIED WARRANTIES WHATSOEVER 
 * WITH RESPECT TO ITS FUNCTIONALITY, OPERABILITY, OR USE, INCLUDING, WITHOUT LIMITATION, ANY IMPLIED 
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, OR INFRINGEMENT. WE EXPRESSLY 
 * DISCLAIM ANY LIABILITY WHATSOEVER FOR ANY DIRECT, INDIRECT, CONSEQUENTIAL, INCIDENTAL OR SPECIAL 
 * DAMAGES, INCLUDING, WITHOUT LIMITATION, LOST REVENUES, LOST PROFITS, LOSSES RESULTING FROM BUSINESS 
 * INTERRUPTION OR LOSS OF DATA, REGARDLESS OF THE FORM OF ACTION OR LEGAL THEORY UNDER WHICH THE LIABILITY 
 * MAY BE ASSERTED, EVEN IF ADVISED OF THE POSSIBILITY OR LIKELIHOOD OF SUCH DAMAGES.
*/


/*
 this is a simple button class. The following shows you how to use it in a minimalistic way.
 
 ADbutton button1;
 boolean pressed=false;
 
 void setup()
 {
  size(290,120);
  smooth();
  
  button1 = new ADbutton(80, 40, 150, 30, 7, "Press Me");

 }
 void buttonRun()
 {
   if (pressed)
     button1.setLabel("Press Me");
   else
     button1.setLabel("unPress Me");
     
   pressed=!pressed;  
   
 }
 void draw()
 {
  background(#04583F);
  if (button1.update())
     buttonRun();
 }
*/
 


public class ADbutton extends ADcontrol
{
  int lightColorButton = 0xFFF2F2F2;
  int darkColorButton = 0xFF505050;
  int innerRectButton = 0xFFB7B7B7;
  int innerRectButtonLight = 0xFFC6C6C6;
  
  int fontColorEnabled=255;
  int fontColorDisabled=0xFF969696;
  
  
  int fontSize = 18;

  boolean visible=true;
  boolean enabled=true;
   
  //actions
  final int mouseNotOver=0;
  final int mouseOver=1;
  final int mouseClick=2;

  boolean mouseButtonPressed=false;
  
  String buttonLabel;
  int buttonX,buttonY,buttonW,buttonH,buttonR,buttonAx,buttonAy,buttonHr;
  
  float startTime;
  boolean didEnter;
  boolean firstTime=true;
  int buttonHelpTime=1500;
  
  int buttonHelpSize=12;
  int buttonHelpColor=0;
  String buttonHelpText="";
  boolean buttonHelp=false;
  
  boolean debug=false;

  Window applet;
  
  // contructor (x,y) (width,Height), radius, label
  public ADbutton(Window applet, int x, int y, int w, int h, int r, String label) 
  {
    this.applet = applet;
    
    buttonX=x;
    buttonY=y;
    buttonW=w;
    buttonH=h;
    buttonR=r;
    buttonAx=buttonX+buttonW-1;
    buttonAy=buttonY+buttonH-1;
    buttonHr=buttonR/2;
    buttonLabel=label;

  }
  
  //internal method
  void display(int action) {

   if (visible) {
	   applet.noStroke();
    applet.rectMode(PApplet.CORNER);

    //Inner applet.rectangle
    switch(action) {
      case mouseNotOver: 
        applet.fill(innerRectButton);
         break;
      case mouseOver: 
        if (enabled)
          applet.fill(innerRectButtonLight);
         else   
           applet.fill(innerRectButton);
        break;
      case mouseClick: 
        applet.fill(innerRectButtonLight);
        break;

    }

    applet.pushMatrix();
    applet.translate(0,0);

    applet.rect(buttonX, buttonY, buttonW, buttonH);

    applet.textAlign(PApplet.CENTER);
    applet.textSize(fontSize);

    switch(action) {
      case mouseNotOver: 

        applet.fill(0xFF939393);
        applet.text(buttonLabel,buttonX+buttonW/2+1,buttonY+buttonH/2+11-5+1);    
        
        if (enabled)
           applet.fill(fontColorEnabled);
        else
           applet.fill(fontColorDisabled);   
        applet.text(buttonLabel,buttonX+buttonW/2,buttonY+buttonH/2+11-5);    
        applet.fill(lightColorButton);
        break;
        
      case mouseOver: 
      
        applet.fill(0xFF939393);
        applet.text(buttonLabel,buttonX+buttonW/2+1,buttonY+buttonH/2+11-5+1);    

        if (enabled)
          applet.fill(fontColorEnabled);
        else
          applet.fill(fontColorDisabled);
        applet.text(buttonLabel,buttonX+buttonW/2,buttonY+buttonH/2+11-5);
        applet.fill(lightColorButton);
        break;
        
      case mouseClick: 
      
        applet.fill(0xFF939393);
        applet.text(buttonLabel,buttonX+buttonW/2+3+1,buttonY+buttonH/2+11-2+1);    
      
        if (enabled)
          applet.fill(fontColorEnabled);
        else
          applet.fill(fontColorDisabled);
        applet.text(buttonLabel,buttonX+buttonW/2+3,buttonY+buttonH/2+11-2);  
        applet.fill(darkColorButton);
        break;  
     }

     //corner up-left
    applet.arc(buttonX, buttonY, buttonR, buttonR, PApplet.radians(180.0f), PApplet.radians(270.0f));
    applet.arc(buttonAx, buttonY, buttonR, buttonR, PApplet.radians(270.0f), PApplet.radians(315.0f));
    applet.arc(buttonX, buttonAy, buttonR,buttonR, PApplet.radians(135.0f), PApplet.radians(180.0f));
 
    applet.rect(buttonX, buttonY-buttonHr, buttonW, buttonHr);
    applet.rect(buttonX-buttonHr, buttonY, buttonHr, buttonH);

    switch(action) {
      case mouseNotOver: 
        applet.fill(darkColorButton);
        break;
      case mouseOver: 
        applet.fill(darkColorButton);
        break;
      case mouseClick: 
        applet.fill(lightColorButton);
        break;
    }
 
    //corner down-right
    applet.arc(buttonAx, buttonAy, buttonR,buttonR, PApplet.radians(0.0f), PApplet.radians(90.0f));
    applet.arc(buttonAx, buttonY, buttonR,buttonR, PApplet.radians(315.0f), PApplet.radians(360.0f));
    applet.arc(buttonX, buttonAy, buttonR,buttonR, PApplet.radians(90.0f), PApplet.radians(135.0f));
 
    applet.rect(buttonX, buttonY+buttonH, buttonW, buttonHr);
    applet.rect(buttonX+buttonW,buttonY,buttonHr, buttonH);

    applet.popMatrix();
    
    }
   }  
///////////////////////////////////////////////////////  
   void setHelpOn(String helpText)
   {
     buttonHelp=true;
     buttonHelpText=helpText;
   }
///////////////////////////////////////////////////////  
   void setHelpTextOff()
   {
     buttonHelp=false;
   }
///////////////////////////////////////////////////////  
   void showHelp()
   {
     applet.textSize(buttonHelpSize);
     applet.textAlign(PApplet.LEFT);

//buttonHelpRectangle

     if (applet.mouseY<(buttonY+(buttonH/2)))
     {
       applet.fill(0xFFFFFFA7);     
       applet.strokeWeight(1);  
       applet.stroke(0);
       if (applet.mouseX>(buttonX+(buttonW/2)))
       {
         applet.rect(applet.mouseX-5,applet.mouseY-buttonH/2-25,applet.textWidth(buttonHelpText)+8,20);
         applet.fill(buttonHelpColor);
         applet.text(buttonHelpText, applet.mouseX, applet.mouseY-buttonH/2-10);
       }
       else
       {
         applet.rect(applet.mouseX-applet.textWidth(buttonHelpText)-5,applet.mouseY-buttonH/2-25,applet.textWidth(buttonHelpText)+8,20);
         applet.fill(buttonHelpColor);
         applet.text(buttonHelpText, applet.mouseX-applet.textWidth(buttonHelpText), applet.mouseY-buttonH/2-10);

       }
     }
     else  
     {
       applet.fill(0xFFFFFFA7);   
       applet.strokeWeight(1);  
       applet.stroke(0);
       if (applet.mouseX<(buttonX+(buttonW/2)))
       {
         applet.rect(applet.mouseX-applet.textWidth(buttonHelpText)-5,applet.mouseY+buttonH/2,applet.textWidth(buttonHelpText)+8,20);
         applet.fill(buttonHelpColor);
         applet.text(buttonHelpText, applet.mouseX-applet.textWidth(buttonHelpText), applet.mouseY+buttonH/2+15);       }
       else
       {
         applet.rect(applet.mouseX-5,applet.mouseY+buttonH/2,applet.textWidth(buttonHelpText)+8,20);
         applet.fill(buttonHelpColor);
         applet.text(buttonHelpText, applet.mouseX, applet.mouseY+buttonH/2+15);
       }
     }
   }
///////////////////////////////////////////////////////  
   void updatePosition()
   {
     
     buttonX=buttonX+applet.mouseX-applet.pmouseX;
     buttonY=buttonY+applet.mouseY-applet.pmouseY;
     buttonAx=buttonX+buttonW-1;
     buttonAy=buttonY+buttonH-1;
    

   }
///////////////////////////////////////////////////////  
   void updatePosition(int deltaX, int deltaY)
   {
     
     buttonX=(buttonX+deltaX);
     buttonY=(buttonY+deltaY);
     buttonAx=buttonX+buttonW-1;
     buttonAy=buttonY+buttonH-1;     
     
   }
///////////////////////////////////////////////////////  
  
   // this function is used to display de button and check if it was pressed. 
   // Returns a boolean (true|false)
   // This function has to be executed in the draw() function to evaluate
   // wether the button has been pressed and for graphical refressing purposes
   
   // If debug=true we can move the button using CTRL+PApplet.LEFT mouse button. The
   // configuration information will me printed at the output window
   boolean update()
   {
     
     if (!visible)
        return false;
             
     if ((applet.mouseX > buttonX-buttonR/2 && applet.mouseX < buttonX+buttonW+buttonR/2) && (applet.mouseY > buttonY-buttonR/2 && applet.mouseY < buttonY+buttonH+buttonR/2))
     {
       didEnter=true;
       if ((applet.mousePressed && applet.mouseButton==PApplet.LEFT))
       {
         
         if (applet.keyPressed && debug==true)
         {
          if (applet.keyCode==PApplet.CONTROL)
          { 
            updatePosition();
            display(mouseOver);
          }
          if (applet.keyCode==PApplet.SHIFT)
          { 
            printGeometry();
            display(mouseOver);
          }
         }   
         else   
         {
           if (enabled)
             display(mouseClick);
           else
             display(mouseNotOver);
           
           if (mouseButtonPressed==false)
           {
              if (enabled)
              {
                 mouseButtonPressed=true;
                 return true;
              }
              else
                 return false;
           }
         }
       }
       else 
       { 
         display(mouseOver);
         mouseButtonPressed=false;
       }
     }
     else
     {
        didEnter=false;
        firstTime=true;
        display(mouseNotOver);
     }

     if (didEnter)
     {
       if (firstTime) 
       { 
         startTime=applet.millis();
       }
       firstTime=false;
       
       if ((applet.millis()-startTime)>buttonHelpTime && buttonHelp)
       {
         showHelp();
       }
     }

     return false; 
   }  

   
   
///////////////////////////////////////////////////////  
   // Allows to hide the button
   void hide()
   {
     visible=false;
   }  
///////////////////////////////////////////////////////  
   // Allows to unhide the button
   void unhide()
   {
     visible=true;
   }
///////////////////////////////////////////////////////  
   // Allows to change the label of the button
   void setLabel(String label)
   {
     buttonLabel=label;
   }
///////////////////////////////////////////////////////  
   // Allows to obtain the current label of the button
   String getLabel()
   {
     return buttonLabel;
   }
///////////////////////////////////////////////////////  
   // Allows to change the font size (in points)
   void setFontSize(int fs)
   {
     fontSize=fs;
   }
///////////////////////////////////////////////////////  
   // Allows to get the font size (in points)
   int getFontSize(int fs)
   {
     return fontSize;
   }
///////////////////////////////////////////////////////  
   // Allows to enable the button
   void enable()
   {
     enabled=true;
   }
///////////////////////////////////////////////////////  
   // Allows to disable the button. When a button is disabled it is visible but it can't be pressed
   void disable()
   {
     enabled=false;
   }
///////////////////////////////////////////////////////  
   // returns if the button is enabled or disabled
   boolean status()
   {
     return enabled;
   }
///////////////////////////////////////////////////////  
   //Allows to change the color of the button in rgb format (use #hexadecimal)
   void setColor(int rgbHexColor)
   {
     innerRectButton=rgbHexColor;
     innerRectButtonLight=rgbHexColor+0xFF0F0F0F;
   }
///////////////////////////////////////////////////////  
   void setDefaultColor()
   {
     innerRectButton=0xFFB7B7B7;
     innerRectButtonLight=innerRectButton+0xFF0F0F0F;
   }
///////////////////////////////////////////////////////  
  void setDebugOn()
  {
    debug=true;
  }
///////////////////////////////////////////////////////  
  void setDebugOff()
  {
    debug=false;
  }
///////////////////////////////////////////////////////  
  void printGeometry()
  {
    System.out.println("button = new ADbutton("+buttonX+", "+buttonY+", "+buttonW+", "+buttonH+", "+buttonR+", \""+buttonLabel+"\");");

  }
}

