package libasoles.processing.board.framework.sketches;

import libasoles.processing.board.framework.Main;
import libasoles.processing.board.framework.utils.windows.ControlWindow;
import libasoles.processing.board.framework.utils.windows.SketchWindow;
import processing.core.PApplet;
import processing.event.KeyEvent;
import processing.event.MouseEvent;

/**
 * Como hacer un sketch: 
 * 
 * . importar todas las dependencias necesarias, o sea crear los imports (con ayuda de Eclipse)
 * . quitar de setup todos los methodos size(), smooth(), framerate(), etc que se setean en Main
 * . anteponer a todos los metodos la palabra "public"
 * . anteponer "project." a cada metodo y variable de processing
 * . a los numeros decimales agregarles una f. Ej: 1.10 pasa a ser 1.10f * 
 * . renombrar "color" por "int"
 * . metodos como println son estáticos y se deben llamar directamente sobre PApplet. Ej: PApplet.println("Hi"); 
 *
 */
public abstract class Sketch{

	public int id; // unique ID
    public String name;
    public String description = "Instrucciones:\n";
    
    protected float timer; // used to vary framerate
    protected float fps; // please don't modify this directly 
    protected float delay; 
    
    public SketchWindow applet;
    protected boolean running = false;
    
    public boolean resetOnStop = true;
        
	protected ControlWindow controlWindow;
	
    public Sketch(SketchWindow applet) {
		
        this.applet = applet;
        controlWindow = ((Main) applet).controlWindow;
       
        if(name == null)
        	name = getClass().getSimpleName(); // default name corresponds to class name
        
        //setFramesPerSecond(30f); // 30fps
                
        System.out.println(name + " initializated");
    }
    
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    
    public void setup() {
    	
    	//setFramesPerSecond(30f); // 30fps  
    	//setupControls(); // draw current sketch gui
    }
    
    /* setup gui controls */
    /*public void setupControls() {
  
    }*/
    
    /**
     * Dummy methods to avoid calling applet.smooth by mistake (because that breaks)
     */
    public void smooth(){}    
    public void smooth(int i){}
    
    /**
     * Dummy method to avoid calling applet.frameRate by mistake (because that breaks)
     */
    public void frameRate(float f){}
    
    /*public void setFramesPerSecond( float fps ){
    	
    	timer = 0;
    	this.fps = fps;
    	delay = 1000/fps; 
    }*/
    
    public void reset() {
    	timer = 0;
    }
    
    public void start() {

		while(controlWindow.inited == false){
			
			 try
			 {    
				System.out.println("Waiting for control window");
				applet.background(255);
				applet.stroke(255);
			    applet.text("Waiting for control window", 200, 200);
			    Thread.sleep(1000);			    
			 }
			 catch(Exception e){
				  
			 }
			 if(applet.millis() > 5000)
				 break;
		}			
    	
	    controlWindow.clearControlKnobs();
        setup();
        //timer = 0;
        running = true;
    }

    public void stop() {
        running = false;
    }

    public void pause() {
    	applet.pause();
        running = false;
    }
    
    public void resume() {
    	applet.resume();
        running = true;
    }
        
    /**
     * All projects must check if running before drawing anything
     */
    public boolean isRunning() {

        return running;
    }
    
    public boolean canDraw(){
    	        
    	if (!isRunning()) {
            return false;
        }
    	
        /*if( applet.millis() > timer ){           
            timer+=delay;
        }*/
                
        return true;
    }
    
    public void draw() {
        
    }

    /*public float getFrameCount(){
    	return timer % fps;
    }*/
    
    public void registerKnob(String name, int range, int initialValue){
    	controlWindow.addControl("knob", name, range, initialValue); // default group is "knob"
    }
    
    public void registerKnob(String name, int range, int initialValue, int knobColor){
    	controlWindow.addControl("knob", name, range, initialValue, knobColor); // default group is "knob"
    }
    
    public void registerKnob(String group, String name, int range, int initialValue){
    	controlWindow.addControl(group, name, range, initialValue);
    	
    }
    public void registerKnob(String group, String name, int range, int initialValue, int knobColor){
    	controlWindow.addControl(group, name, range, initialValue, knobColor);
    }
    
    public float getKnobValue(String controlName){
    	return getKnobValue("knob", controlName);
    }
    
    public float getKnobValue(String group, String controlName){
    	return controlWindow.getControlValue(group, controlName);
    }
    
    public void hideKnobGroup(String group){
    	controlWindow.hideControlKnobGroup(group);
    }
    
    public void showKnobGroup(String group){
    	controlWindow.showControlKnobGroup(group);
    }
    	
	protected void displayError(String errorMessage){

		applet.background(255, 0, 0);
		applet.fill(0);
		applet.textAlign(PApplet.CENTER);
		applet.text(errorMessage, applet.width/2, applet.height/2);
		pause();
		return;
	}
    
    @Override
    public String toString() {
        return name;
    }
    
    /* events with param */
	public void keyPressed(KeyEvent event) {				
	}
	public void keyReleased(KeyEvent event) {
	}
	public void keyTyped(KeyEvent event) {
	}
	public void mouseClicked(MouseEvent event) {
	}
	public void mouseDragged(MouseEvent event) {
	}
	public void mouseEntered(MouseEvent event) {
	}
	public void mouseExited(MouseEvent event) {
	}
	public void mouseMoved(MouseEvent event) {
	}
	public void mousePressed(MouseEvent event) {
	}
	public void mouseReleased(MouseEvent event) {
	}
	public void mouseWheel(MouseEvent event) {
	}

	/* events without param */
	public void keyPressed() {	
	}
	public void keyReleased() {
	}
	public void keyTyped() {
	}
	public void mouseClicked() {
	}
	public void mouseDragged() {
	}
	public void mouseEntered() {
	}
	public void mouseExited() {
	}
	public void mouseMoved() {
	}
	public void mousePressed() {
	}
	public void mouseReleased() {
	}
	public void mouseWheel() {
	}
}
