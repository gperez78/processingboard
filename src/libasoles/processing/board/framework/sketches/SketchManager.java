package libasoles.processing.board.framework.sketches;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;

import libasoles.processing.board.framework.utils.windows.SketchWindow;
import libasoles.processing.board.framework.utils.windows.Window;
import processing.event.KeyEvent;
import processing.event.MouseEvent;

public class SketchManager{

	SketchWindow sketchWindow;
    ArrayList<Sketch> sketches;    
    Sketch currentSketch;
    
    public SketchManager() {
        sketches = new ArrayList<Sketch>();
    }
    
    public SketchManager(SketchWindow window) {
    	this();
    	sketchWindow = window;
    }
    
    public void add(Sketch s){
        sketches.add(s);
        s.id = sketches.indexOf(s);
    }
    
    public void delete(){
        
    }
    
    public Sketch get(int i){
        return sketches.get(i);
        
    }
    
    public ArrayList<Sketch> getAll(){
        return sketches;
    }

    public Sketch getCurrentSketch() {
        return currentSketch;
    }

    public void setCurrentSketch(Sketch sketch) {
        this.currentSketch = sketch;
    }
    
    public boolean setCurrentSketch(int sketchID) {
    	    	
    	try{
    		
    		if(currentSketch != null){
        		currentSketch.stop();
        		if(currentSketch.resetOnStop){    	
        			int index = sketches.indexOf(currentSketch);
        			resetCurrentSketch( index );
        		}       		
        	}
        	
        	this.currentSketch = this.get(sketchID);
        	
        	if(currentSketch != null){
        		playCurrentSketch();
        		System.out.println("Current sketch: " + currentSketch.getName() );
        		return true;
        	}
        	
    	} catch(Exception e){
    		System.out.println("Error loading sketch");
    	}
    	return false;
    }
    
    public void setCurrentSketch(String sketchName) {
    	
    	if(currentSketch != null) {
    		
    		currentSketch.stop();
    		
    		if(currentSketch.resetOnStop){
    			int index = sketches.indexOf(currentSketch);
    			resetCurrentSketch( index );
    		}   			
    	}
    	
    	for(Sketch s : sketches){
    		if(s.getName().equals(sketchName)){
    			System.out.println("Gotcha " + s.getName());
    			currentSketch = s;
    			break;
    		}
    	}
    	
    	if(currentSketch != null){
    		playCurrentSketch();
    		System.out.println("Current sketch: " + currentSketch.getName() );
    	}
    }
    
    private void resetCurrentSketch(int index){
    	
    	try {
			// re-instantiate current sketch (will only work if class accepts one param, the SketchWindow)
			
			String className = currentSketch.getClass().getName();    				
			Class<?> cl = Class.forName(className);    				
			Constructor<?> con = null;
			try {
				con = cl.getConstructor(SketchWindow.class);
			} catch (NoSuchMethodException e1) {	
				e1.printStackTrace();
			} catch (SecurityException e1) {
				e1.printStackTrace();
			}    		
			try {
				System.out.println("killing " + currentSketch.getClass().getSimpleName() + ". New instance will replace it.");
				Sketch tmp = (Sketch) con.newInstance((SketchWindow)currentSketch.applet);
				sketches.set(index, tmp);				
			} catch (IllegalArgumentException e2) {
				e2.printStackTrace();
			} catch (InvocationTargetException e2) {
				e2.printStackTrace();
			} 
			
		} catch (InstantiationException e) {				
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} 
    }
    
    public void playCurrentSketch(){
        currentSketch.start();
    }
    
 
    public void playNextSketch(){
     
    	int sketchIndex = currentSketch.id;
		if(++sketchIndex > sketches.size())
			sketchIndex = 1;
		
		setCurrentSketch( sketchIndex - 1 );
    }
    
    public void draw(){
    	
        if(currentSketch != null && currentSketch.canDraw()){
        	
        	currentSketch.draw();
        	
        	if (currentSketch.applet instanceof SketchWindow){
        		
        		synchronized (Window.mirror) {
            		
            	    Window.mirror = currentSketch.applet.get();
            	}
        	}
        	
        }            
    }
    
	public void keyPressed(KeyEvent event) {
		
		if(currentSketch != null){
			
			switch(event.getKey()){
			
				case ' ':
					playNextSketch();
					break;					
				case 'p':
				case 'P':
					if(currentSketch.isRunning())
						currentSketch.pause();
					else
						currentSketch.resume();
					break;							
				default:
					currentSketch.keyPressed(event);
			    	currentSketch.keyPressed();
			    	break;					
			}					
		}
	}
		
	public void keyReleased(KeyEvent event) {
		if(currentSketch != null){
			currentSketch.keyReleased(event);
			currentSketch.keyReleased();
		}
	}
	public void keyTyped(KeyEvent event) {
		if(currentSketch != null){
			currentSketch.keyTyped(event);
			currentSketch.keyTyped();
		}
	}
	public void mouseClicked(MouseEvent event) {
		if(currentSketch != null){		
			currentSketch.mouseClicked(event);
			currentSketch.mouseClicked();
		}
	}
	public void mouseDragged(MouseEvent event) {
		if(currentSketch != null){
			currentSketch.mouseDragged(event);
			currentSketch.mouseDragged();
		}
	}
	public void mouseEntered(MouseEvent event) {
		if(currentSketch != null){
			currentSketch.mouseEntered(event);
			currentSketch.mouseEntered();
		}
	}
	public void mouseExited(MouseEvent event) {
		if(currentSketch != null){
			
			currentSketch.mouseExited(event);
			currentSketch.mouseExited();
		}
	}
	public void mouseMoved(MouseEvent event) {
		if(currentSketch != null){
			currentSketch.mouseMoved(event);
			currentSketch.mouseMoved();
		}
	}
	public void mousePressed(MouseEvent event) {
		if(currentSketch != null){
			currentSketch.mousePressed(event);
			currentSketch.mousePressed();
		}
	}
	public void mouseReleased(MouseEvent event) {
		if(currentSketch != null){
			currentSketch.mouseReleased(event);
			currentSketch.mouseReleased();
		}		
	}
	public void mouseWheel(MouseEvent event) {
		if(currentSketch != null){
			currentSketch.mouseWheel(event);
			currentSketch.mouseWheel();
		}		
	}
}