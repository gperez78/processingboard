# README #

Processing (2.x) based project to manipulate sketches on the run using knob controls.

This project renders two windows. First one is a control board. Second one is the sketch itself.

![alt tag](https://s32.postimg.org/zakeoh8ph/Screenshot_20160724_123336.png)

### Content ###

* This acts like a framework to create processing based projects in Eclipse, IntelliJ or your chosen IDE. It basically provides you with knob controls for your sketches. And it was developed to project an image on a wall while manipulating variables via a knobs based control board.

### How to make it run ###

* Load the project in Eclipse or your favorite IDE
* Import the base libraries from lib folder
* Launch the DemoProject

### Things you can do ###

* You can interact with your sketches with your mouse using the control video area.
* You can define knob controls (credits to Alejandro Dirgan. See https://www.youtube.com/watch?v=v3pIN3FNqHk)
* You can load a List of sketches and make them run in a queue.
* Each sketch can define a description or instructions that you can see in the control board.

### Release ###

* This is a working copy, but it's considered beta. If you want to collaborate you are welcome.